# -*- coding: utf-8 -*-

# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# February 2021


"""
**A Genetic algorithm approach to solving linear regression**

Linear regression can be analytically solved by matrix calculus. However, it's a problem in which we can be approximately correct, hence a good example for demonstrating how genetic algorithms work. 

In any regression problem, we have some elements :

- Inputs : these are the variables used in predicting outputs

- Outputs : these are the variables we wish to predict

- Parameters : values that define relationships between inputs and ouputs. We try to predict these with as much accuracy as possible

If using a genetic algorithm to solve a problem :

- An individual can be considered a possible solution

- A population will be a collection of possible solutions

- A fitness measure is a way of measuring how good a solution is

- A generation is a level of advancement towards the disred fitness

- A variation operation is a change to an individual to alter its output

First, we import necessary modules and needed functions :
"""

# Most credit for : https://medium.com/the-andela-way/on-genetic-algorithms-and-their-application-in-solving-regression-problem-4e37ac1115d5
from random import random, sample, choice
from math import floor
from tqdm import tqdm # Small library for viewing progress of a loop
from numpy import array, dot, mean # Matrix manipulations
from numpy.linalg import pinv # Numpy is mainly used for matrix manipulation. These are only non standard libraries used

"""We will generate data with a clear pattern. This ensures we have an idea of the desired result."""

coeff = [0.4,-0.3,0.2,-0.1]
def generate_data():
    x = [[random() for j in range(len(coeff))] for i in range(100)] # Allows to compress the code
    y = [dot(i, coeff) for i in x] # It's called a "list comprehension"
    return array(x), array(y)

"""Let's now generate the data :"""

inputs, outputs = generate_data()

"""Then, we write a function to demonstrate how linear regression would be analytically solved. The solution returned should equal the coefficients used to generate the data.

- SSR : the total squared error in a model

- error : the average error
"""

def multiple_linear_regression(inputs, outputs):
    X, Y = array(inputs), array(outputs)
    X_t = X.transpose()
    coeff = dot((pinv((dot(X_t, X)))), (dot(X_t, Y)))
    Y_p = dot(X, coeff)
    SSR = array([(i-j) ** 2 for i, j in zip(Y, Y_p)]).sum()
    av_error = (SSR/len(Y))
    return {'coeff': coeff, 'av_error': av_error}

print(multiple_linear_regression(inputs, outputs))

"""One of the first requirements of a genetic algorithm is a termination condition. So we write a function to check whether we currently meet any of the termination conditions."""

def check_termination_condition(threshold, best_individual, generation_count, max_generations):
    if ((best_individual['av_error'] <= threshold)
         or (generation_count == max_generations)):
        return True
    else:
        return False

"""To create an initial individual, we will used random assigning of variables. The individual size is just the number of input variables."""

def create_individual(individual_size):
    return [random() for i in range(individual_size)]

"""We then use this function to create an initial population :"""

def create_population(individual_size, population_size):
    return [create_individual(individual_size) for i in range(population_size)]

"""To get the fitness of an individual, we make predictions with it and return its error and coefficients."""

def get_fitness(individual, inputs):
    predicted_outputs = dot(array(inputs), array(individual))
    SSR = array(
        [(i-j) ** 2 for i, j in zip(outputs, predicted_outputs)]
        ).sum()
    average_error = (SSR / len(outputs))
    return {'av_error': average_error, 'coeff': individual}

"""We then use this function to evaluate a population and return the best individuals. We chose to use least errors for simplicity in selectiong the best individuals."""

def evaluate_population(population):
    fitness_list = [get_fitness(individual, inputs) for individual in tqdm(population)]
    error_list = sorted(fitness_list, key = lambda i: i ['av_error'])
    best_individuals = error_list[: selection_size]
    best_individual_stash.append(best_individuals[0]['coeff'])
    print('Average error : ', best_individuals[0]['av_error'])
    return best_individuals

"""Variation operations need to be applied to evolve a population. We will use crossover and mutation.

In crossover, two parents provide the genes for a child, each giving half.
"""

def crossover(parent_1, parent_2):
    child = {}
    loci = [i for i in range(0, individual_size)]
    loci_1 = sample(loci, floor(0.5*(individual_size)))
    loci_2 = [i for i in loci if i not in loci_1]
    chromosome_1 = [[i, parent_1['coeff'][i]] for i in loci_1]
    chromosome_2 = [[i, parent_2['coeff'][i]] for i in loci_2]
    child.update({key: value for (key, value) in chromosome_1})
    child.update({key: value for (key, value) in chromosome_2})
    return [child[i] for i in loci]

"""In mutation, a selected number of genes in a selected number of individuals is randomly changed."""

def mutate(individual):
    loci = [i for i in range(0, individual_size)]
    no_of_genes_mutated = floor(probability_of_gene_mutating*individual_size)
    loci_to_mutate = sample(loci, no_of_genes_mutated)
    for locus in loci_to_mutate:
        gene_transform = choice([-1,1])
        change = gene_transform*random()
        individual[locus] = individual[locus] + change
    return individual

"""With all these, we need to be able to create a new generation of individuals from a small group of individuals to advance a population forward.

Its made to be of equal size with the original population but can be randomized.
"""

def get_new_generation(selected_individuals):
    parent_pairs = [sample(selected_individuals, 2) for i in range(population_size)]
    offspring = [crossover(pair[0], pair[1]) for pair in parent_pairs]
    offspring_indices = [i for i in range(population_size)]
    offspring_to_mutate = sample(
        offspring_indices,
        floor(probability_of_individual_mutating*population_size)
        )
    mutated_offspring = [[i, mutate(offspring[i])] for i in offspring_to_mutate]
    for child in mutated_offspring:
        offspring[child[0]] = child[1]
    return offspring

"""Finally, we now implement the logic that actually runs the functions. The values assigned to the variables can be altered to experiment with different scenarios :"""

individual_size = len(inputs[0])
population_size = 1000
selection_size = floor(0.1*population_size)
max_generations = 100
threshold = 0.00
probability_of_individual_mutating = 0.1
probability_of_gene_mutating = 0.25

best_possible = multiple_linear_regression(inputs, outputs)
best_individual_stash = [create_individual(individual_size)]

initial_population = create_population(individual_size, 1000)
current_population = initial_population
termination = False
generation_count = 0
errors = []
while termination is False:
    current_best_individual = get_fitness(best_individual_stash[-1], inputs)
    print('Generation : ', generation_count)
    best_individuals = evaluate_population(current_population)
    current_population = get_new_generation(best_individuals)
    termination = check_termination_condition(threshold, current_best_individual, generation_count, max_generations)
    generation_count += 1
    errors.append(best_individuals[0]['av_error'])
else:
    print(get_fitness(best_individual_stash[-1], inputs))

print(best_possible)
