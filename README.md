**A Genetic algorithm approach to solve linear regression**

**Step 1**

Generate the data.

**Step 2**

Write a function to demonstrate how linear regression would be analytically solved. The solution returned should equal the coefficients used to generate the data.

**Step 3**

Write a function to check whether we currently meet any of the termination conditions.

**Step 4**

To create an initial individual, we will used random assigning of variables. The individual size is just the number of input variables. We then use a function to create an initial population.

**Step 5**

To get the fitness of an individual, we make predictions with it and return its error and coefficients.

**Step 6**

We then use this function to evaluate a population and return the best individuals. We chose to use least errors for simplicity in selectiong the best individuals.

**Step 7**

Variation operations need to be applied to evolve a population. We will use crossover and mutation.

**Step 8**

We implement the logic that actually runs the functions. The values assigned to the variables can be altered to experiment with different scenarios.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

February 2021






















